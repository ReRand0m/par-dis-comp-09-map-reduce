#! /usr/bin/env bash

mvn package

hadoop jar target/StackExchangeCountHistograms-1.0.jar org.example.StackExchangeCountHistogramRunner /data/stackexchange_part/users /data/stackexchange_part/posts out
