#! /usr/bin/env bash

mvn package
hdfs dfs -rm -r -skipTrash wordcount_out 2> /dev/null
# hadoop jar jar/WordCount.jar org.example.WordCount /data/wiki/en_articles_part wordcount_out
yarn jar target/WordCount-1.0.jar org.example.WordCount /data/wiki/en_articles_part wordcount_out